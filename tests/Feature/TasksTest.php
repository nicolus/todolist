<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class TasksTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testShowsTasks()
    {
        $user = User::factory()->create();
        $user->tasks()->save(new Task(['description' => "Do the laundry"]));

        $this->actingAs($user);
        $response = $this->get('/tasks/');

        $response->assertStatus(200);
        $response->assertSeeText("Do the laundry");
    }
}
